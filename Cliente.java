package videos;

	public class Cliente {
		
	int id_cl;
	String nombre;
	String membresia;
	Video [] videos;
	int videoIndex;
	
	public Cliente (int id_cl, String nombre, String membresia){
		this.id_cl=id_cl;
		this.nombre=nombre;
		this.membresia=membresia;
		videos= new Video[20];
		videoIndex = 0;
		
	}
	
	public void rentar(Video video){
		
		videos[videoIndex] = video;
		videoIndex++;
	}
	
	public float totalRenta(){
		float sumatoria=0;
		
		for(int i =0; i<videoIndex;i++){
			sumatoria+=videos[i].precio;
		}
		
		return sumatoria;
	}
	
	public void mostrarRentas(){
		
		for(int i=0;i<videoIndex;i++){
			
			System.out.println("- "+ videos[i].nombre+ "  $ " +videos[i].precio);
		
		}
	}
}

