package videos;

import java.util.*;
import java.util.Scanner;

public class Main {
	
	static Cliente [] clientes = new Cliente[10];
	static Video [] videos = new Video[20];
	static int clienteIndex = 0;
	static int videoIndex= 0;
	static Scanner leer = new Scanner(System.in); //Sirve para recoger texto por consola
	static int op=0; //opci�n elegida del usuario
	
	    public static void main(String [] args){
	    	
	    	videos [0] = new Video (1,"Chupacabras", 100 ,"Adulto");
			videos [1] = new Video (2,"Bambirrias", 50 ,"Infantil");
			videos [2] = new Video (3,"Toys Love", 110 ,"Infantil");
			videos [3] = new Video (4,"Dura de matar", 50 ,"Adulto");
			videos [4] = new Video (5,"Verano en Miami", 100 ,"Adolescente");
			videos [5] = new Video (6,"Ameli", 180 ,"Adolescente");
			videos [6] = new Video (7,"Asesinadas ", 20 ,"Adulto");
			videos [7] = new Video (8,"9 balas", 65 ,"Adulto");
			videos [8] = new Video (9,"009 ", 100 ,"Adulto");
			videos [9] = new Video (10,"killbill", 65 ,"Adulto");
			videos [10] = new Video (11,"indestructibles", 65 ,"Adolescente");
			videos [11] = new Video (12,"Hora de aventura", 100 ,"Infantil");
			
			clientes [0] = new Cliente (1,"juan","gold");
			clientes [1] = new Cliente (2,"Carlos","normal");
			clientes [2] = new Cliente (3,"Benito","normal");
			
			while(true){
				
					System.out.println("Seleccione una opcion:");
					System.out.println("  ");
					System.out.println("1.-Rentar Video ");
					System.out.println("2.-Calcular el total de la renta para 1 cliente");
					System.out.println("3.-Calcular el total de renta de Videos al d�a");
					System.out.println("4.-Calcular Total de renta por membrecia.");
					System.out.println("0. -Salir.");
					op = leer.nextInt();
					
					if (op==0)
					break;
							
									//switch case
									switch(op){
									case 1:{
										rentarVideo();
										break;}
									case 2:{ 
										int idCliente= seleccionarCliente();
										System.out.println("Total de: "+clientes[idCliente].nombre);
										System.out.println("$"+clientes[idCliente].totalRenta());
										break;}
									case 3: 
									{
										float sumatoria=0;
										
										for(int i=0;i<clienteIndex;i++)
											sumatoria+=clientes[i].totalRenta();
								
										System.out.println("Total del dia: $"+sumatoria);
										break;
												}
									case 4: {
										float sumaNormal=0, sumaGold=0;
										
										for(int i=0;i<clienteIndex;i++){
											if(clientes[i].membresia=="normal")
												sumaNormal+=clientes[i].totalRenta();
											else
												sumaGold+=clientes[i].totalRenta();
										}
					
										System.out.println("Total de renta por membresia normal $"+sumaNormal);
										System.out.println("Total de renta por membresia gold $"+sumaGold);
										
										break;}
									case 0: 
										System.out.println("Adios!");
										break;
									default:
										System.out.println("N�mero no reconocido");
										
					}				
				}
		}
	    
	    
	    	public static int seleccionarCliente(){
			for(int i=0; i<clienteIndex; i++){
				Cliente cte = clientes[i];
				System.out.println("id:"+cte.id_cl+".- "+cte.nombre);
			}
			
			System.out.println("Capture el id del cliente");
			int id = leer.nextInt();
			
			return id-1;
		}
	    
	    	public static int seleccionarVideo(){
	    		for(int i=0; i<videoIndex; i++){
	    			Video vid = videos[i];
	    			System.out.println("id:"+idv.id+".- "+idv.nombre +"\t\t$"+idv.precio);
	    		}
	    		
	    		System.out.println("Capture el id del video");
	    		int id = leer.nextInt();
	    		
	    		return id-1;
	    	}
	    	
			public static void rentarVideo(){
			
			int idCliente = seleccionarCliente();
			int idVideo = seleccionarVideo();
			
			clientes[idCliente].rentar(videos[idVideo]);
			clientes[idCliente].mostrarRentas();
		}
}
