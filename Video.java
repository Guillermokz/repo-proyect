package videos;

public class Video {
	int id;
	String nombre;
	float precio;
	String tipo;
	
	public Video (int id, String nombre, float precio, String tipo){
		this.id=id;
		this.nombre=nombre;
		this.precio=precio;
		this.tipo=tipo;
	}
}
